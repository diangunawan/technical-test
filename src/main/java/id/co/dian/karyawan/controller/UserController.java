package id.co.dian.karyawan.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.co.dian.karyawan.dto.UserDto;
import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.model.User;
import id.co.dian.karyawan.repository.UserRepository;
import id.co.dian.karyawan.repository.UserRepository.getAl_Users;
import id.co.dian.karyawan.service.GroupService;
import id.co.dian.karyawan.service.UserService;
import id.co.dian.karyawan.service.UserServiceImpl;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserServiceImpl userImpl;

	@GetMapping("/")
	public String getAll(Principal principal, Model model) {
		return "layout";

	}

	@GetMapping("/viewIndex")
	public String getAllAsComponent(Principal principal, Model model) {
		List<Group> group = groupService.findAll();
		return "preferensi/user/index";

	}

	@GetMapping("/add")
	public String regist(Principal principal, Model model) {
    	return "layout";
	}

	@GetMapping("/add/viewIndex")
	public String getAddContent(Model model) {
		List<Group> group = groupService.findAll();
		return "preferensi/user/add";
	}

	@GetMapping(value = "/edit/{id}")
	public String edit(Principal principal, Model model, @PathVariable("id") String id) {
		return "layout";
	}

	@GetMapping(value = "/edit/{id}/viewIndex")
	public String getEditContent(@PathVariable("id") String id, Model model) {
		List<getAl_Users> users = userRepository.getForEdit(Integer.parseInt(id));
		String username = "";
		String idGroup = "";
		String nameGroup = "";
		String telp = "";
		String email = "";
		if(!users.isEmpty()) {
			for(getAl_Users val : users) {
				username = val.getUsername();
				idGroup = val.getIdGroup();
				nameGroup = val.getNameGroup();
				telp = val.getTelp();
				email = val.getEmail();
			}
		}
		return "preferensi/user/edit";
	}

	@PostMapping("/save")
	public String create(Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String username = principal.getName();
		User user = userService.findByUsername(username);
		UserDto dto = new UserDto();
		Integer id = request.getParameter("id").equals("") ? null : Integer.parseInt(request.getParameter("id"));
		dto.setIdUsers(id);
		dto.setUsername(request.getParameter("username").trim());
		dto.setPassword(request.getParameter("new_password").trim());
		dto.setEmail(request.getParameter("email").trim());
		dto.setTelp(request.getParameter("telp").trim());
		dto.setUserCreated(user.getIdUsers());
		dto.setIdGroup(Integer.parseInt(request.getParameter("group").trim()));

		String newPass = request.getParameter("new_password");
		String confirmPass = request.getParameter("confirm_password");
		if (newPass.equals(confirmPass)) {
			redirectAttributes.addFlashAttribute("message", "Sukses Simpan Data Pengguna.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		} else {
			redirectAttributes.addFlashAttribute("message", "Gagal Simpan! password salah.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
			if(id == null) {
				return "redirect:/user/add/viewIndex";
			}
			return "redirect:/user/edit/"+id.toString()+"/viewIndex";
		}
	}

	@GetMapping("/delete/{id}/viewIndex")
	public String delete(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
		try {
			Integer delete = userImpl.delete(Integer.parseInt(id));
			if(delete == 1) {
				redirectAttributes.addFlashAttribute("message", "Sukses Menghapus Pengguna.");
				redirectAttributes.addFlashAttribute("alertClass", "alert-success");
				return "redirect:/user/viewIndex";
			}
			redirectAttributes.addFlashAttribute("message", "Gagal Menghapus Data Pengguna.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		}catch(Exception e){
			redirectAttributes.addFlashAttribute("message", "error exception.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		}
	}

	@GetMapping("/checkUserName")
	@ResponseBody
	public boolean  checkUser(@RequestParam("userName") String userName, RedirectAttributes redirectAttributes){

		User user = null;
		if (user != null) {

			 return true ;// method return bolean if user exist or non in database.
		}else {
			return false;
		}
	}

}
