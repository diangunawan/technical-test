package id.co.dian.karyawan.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;

import id.co.dian.karyawan.model.Employee;
import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.model.Position;
import id.co.dian.karyawan.service.EmployeeService;
import id.co.dian.karyawan.service.GroupService;
import id.co.dian.karyawan.service.PositionService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private GroupService groupService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PositionService positionService;

	@GetMapping("/")
	public String getAll(Principal principal, Model model) {
		return "layout";

	}

	@GetMapping("/viewIndex")
	public String getAllAsComponent(Principal principal, Model model) {
		List<Group> group = groupService.findAll();
		List<Employee> listData = employeeService.getAktif();
		List<Position> listJab = positionService.getAll();


		return "employee/index";

	}

	@GetMapping("/add")
	public String regist(Principal principal, Model model) {
    	return "layout";
	}

	@GetMapping("/add/viewIndex")
	public String getAddContent(Model model) {
		List<Group> group = groupService.findAll();
		model.addAttribute("group", group);
		return "employee/add";
	}

	@GetMapping(value = "/edit/{id}")
	public String edit(Principal principal, Model model, @PathVariable("id") String id) {
		return "layout";
	}

	@GetMapping("/checkNip")
	@ResponseBody
	public boolean  checkUser(@RequestParam("nip") String nip, RedirectAttributes redirectAttributes){
		Integer emp= null;
		if (emp != null) {

			 return true ;// method return bolean if user exist or non in database.
		}else {
			return false;
		}
	}

	@PostMapping("/save")
	public String create(Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception {

		try {
			SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy");
			String cekDate = request.getParameter("birthDate").trim();
			Date date = formatter.parse(request.getParameter("birthDate").trim());
			Employee emp = new Employee();
			emp.setId(request.getParameter("id").equals("") ? null : Integer.parseInt(request.getParameter("id")));
			emp.setName(request.getParameter("name").trim());
			emp.setBirthDate(date);
			emp.setGender(Integer.parseInt(request.getParameter("gender")));
			emp.setPositionId(Integer.parseInt(request.getParameter("jabatan")));
			emp.setIdNumber(Integer.parseInt(request.getParameter("idNumber")));

			Employee cekUniq = employeeService.getByNip(emp.getIdNumber());

			if (cekUniq != null && emp.getId() == null) {
				redirectAttributes.addFlashAttribute("message",
						"Gagal Simpan! Referensi '" + emp.getName() + "' sudah ada.");
				redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
				return "redirect:/employee/viewIndex";

			} else if (cekUniq != null && cekUniq.getId() != emp.getId()) {
				redirectAttributes.addFlashAttribute("message",
						"Gagal Simpan! Referensi '" + emp.getName() + "' sudah ada.");
				redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
				return "redirect:/employee/viewIndex";

			} else {
				employeeService.save(emp);

				redirectAttributes.addFlashAttribute("message", "Sukses Simpan Data Karyawan.");
				redirectAttributes.addFlashAttribute("alertClass", "alert-success");
				return "redirect:/employee/viewIndex";
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@PostMapping("/delete")
	public String delete(HttpServletRequest request) {
		employeeService.delete(Integer.parseInt(request.getParameter("id")));

		return "redirect:/employee/viewIndex";
	}
}
