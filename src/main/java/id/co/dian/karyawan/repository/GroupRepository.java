package id.co.dian.karyawan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.repository.UserRepository.getAl_Users;

public interface GroupRepository extends JpaRepository<Group, Integer>{
	
	@Query("FROM Group a where a.idGroup=:idGroup")
	Group getById(Integer idGroup);
	
	@Query(value="select max(id_group) from al_group",nativeQuery=true)
	Integer getLastId();
	
	@Query(value="SELECT count(id_group) as total FROM al_group",nativeQuery=true)
	Integer getTotal();
	
	@Query(value="SELECT id_group as id, name_group as nameGroup, desc_group as descGroup \r\n"+
			"FROM al_group as a\r\n"+
			"WHERE id_group=:id", nativeQuery = true)
	List<getAl_Group> getForEdit(Integer id);
	
	public static interface getAl_Group {
		String getId();
		String getNameGroup();
		String getDescGroup();
	}
}
