package id.co.dian.karyawan.model;



import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
public class Group {


	private int idGroup;
	private String nameGroup;
	private String descGroup;
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	private int userCreated;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	private int userUpdated;

	public Group() {

	}

}
