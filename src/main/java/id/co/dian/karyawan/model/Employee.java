package id.co.dian.karyawan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
@Entity
public class Employee {

	private Integer id;


	@NotEmpty
	@Column(length = 100)
	private String name;


	private Date birthDate;

	private int positionId;

	@Column(unique = true, length = 18)
	private int idNumber;

    private int gender;

    private int isDelete = 0;

    public Employee() {

    }
}
