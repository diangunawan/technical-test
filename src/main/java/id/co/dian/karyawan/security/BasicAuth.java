package id.co.dian.karyawan.security;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.dian.karyawan.model.User;
import id.co.dian.karyawan.service.UserService;

@Service
public class BasicAuth {
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	public static String base64de(String param) {
		try {
			Base64.Decoder decoder = Base64.getDecoder();

			String dStr = new String(decoder.decode(param));
			return dStr;
		} catch (Exception ex) {
			ex.printStackTrace();
		}	
		return param;
	}
	
	public Boolean cekAuth(String param) {
		String decod = base64de(param.replace("Basic ", ""));
		
		String[] usPas = decod.split(":");
		
		User us = userService.findByUsername(usPas[0]);
		
		if(us != null) {
			if(passwordEncoder.matches(usPas[1], us.getPassword())) {
				return true;
			}
		}else {
			return false;
		}
		
		return false;
	}
}
