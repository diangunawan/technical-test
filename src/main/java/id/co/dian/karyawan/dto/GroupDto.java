package id.co.dian.karyawan.dto;

import java.util.Date;

import lombok.Data;

@Data
public class GroupDto {
	private int idGroup;
	private String nameGroup;
	private String descGroup;
	private Date created;
	private int userCreated;
	private Date updated;
	private int userUpdated;
}
