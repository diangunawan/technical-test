package id.co.dian.karyawan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CustomizedUnAuthorizedException extends RuntimeException {

	public CustomizedUnAuthorizedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

}
