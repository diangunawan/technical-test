package id.co.dian.karyawan.service;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.dian.karyawan.dto.GroupDto;
import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.model.User;
import id.co.dian.karyawan.repository.GroupRepository;
import id.co.dian.karyawan.repository.UserRepository;
import id.co.dian.karyawan.sql.ConnectionSqlServer;

@Service
public class GroupServiceImpl extends ConnectionSqlServer implements GroupService {
	
	@Autowired
	private GroupRepository groupRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private UserRepository userRepository;

	@Override
	public Group getById(Integer idGroup) {
		return groupRepo.getById(idGroup);
	}

	@Override
	public List<Group> findAll() {
		return groupRepo.findAll();
	}
	
	public Integer getCountFilter(String filterSearch) {
		Integer rest = 0;
		try {
			connection = getConnection();
			String queryFilterSearch = " ";
			if(filterSearch != "") {
				queryFilterSearch = " AND name_group LIKE '%"+filterSearch+"%' ";
			}
			
			PreparedStatement query = connection.prepareStatement("SELECT COUNT(id_group) as total \r\n"+
					"FROM al_group \r\n"+
					"WHERE 1=1 "+queryFilterSearch);
			
			rs = query.executeQuery();
			while (rs.next()) {
				rest = Integer.parseInt(rs.getString("total"));
			}
			//System.out.println("total Filter "+result);
			return rest;
		}catch(HibernateException e){
			System.out.println(e);
			return rest;
		}catch(Exception e){
			System.out.println(e);
			return rest;
		}
	}
	
	public List<Object> getGroups(String filterSearch, Integer start, Integer length) {
		List<Object> rest = new ArrayList<Object>();
		try {
			connection = getConnection();
			String queryFilterSearch = " ";
			if(!filterSearch.isEmpty()) {
				queryFilterSearch = " AND name_group LIKE '%"+filterSearch+"%' ";
			}
			
			PreparedStatement query = connection.prepareStatement("SELECT id_group as id, name_group as nameGroup, desc_group as descGroup \r\n"+
					"FROM al_group \r\n"+
					"WHERE 1=1 "+queryFilterSearch+
					"ORDER BY created DESC OFFSET ? rows FETCH NEXT ? ROWS ONLY");
			query.setInt(1, start);
			query.setInt(2, length);
			// show query error
			//System.out.println(query.toString());
			rs = query.executeQuery();
			
			Integer no = start;
			while (rs.next()) {
				Map<String,String> res = new HashMap<String,String>();
				no = no+1;
				res.put("no", no.toString());
				res.put("groupName", rs.getString("nameGroup"));
				res.put("desc", rs.getString("descGroup"));
				String btnEdit = "<a href=\"/role/edit/"+rs.getString("id")+"/\" class=\"btn btn-xs btn-success ajax\"><span class=\"fa fa-pencil\"></span></a>";
				String btnDelete = "<a data-href=\"/role/delete/"+rs.getString("id")+"/\" class=\"btn btn-xs btn-warning btn-delete\" title=\"Hapus data\" data-confirm=\"Anda yakin ingin menghapus "+rs.getString("nameGroup")+"?\"><span class=\"fa fa-trash text-white\"></span></a>";
				res.put("action", "<div class=\"btn-group\">"+btnEdit+btnDelete+"</div>");
				
				rest.add(res);
			}
			return rest;
		}catch(HibernateException e){
			System.out.println(e);
			return rest;
		}catch(Exception e){
			System.out.println(e);
			return rest;
		}
	}

	@Override
	public Group save(GroupDto group) {
		Group grb = new Group();
		Integer id = groupRepo.getLastId();
		grb.setIdGroup(group.getIdGroup() == null ? id+1 : group.getIdGroup());
		grb.setNameGroup(group.getNameGroup());
		grb.setDescGroup(group.getDescGroup());
		grb.setCreated(new Date());
		grb.setUserCreated(group.getUserCreated());
		return groupRepo.save(grb);
	}
	
	public Integer delete(Integer id) {
		try {
			connection = getConnection();
			PreparedStatement query = connection.prepareStatement("DELETE FROM al_group WHERE id_group="+id);
			Integer result = query.executeUpdate();
			//System.out.println(result);
			return result;
		}catch(Exception e){
			System.out.println(e);
			return 0;
		}
	}

}
