package id.co.dian.karyawan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.dian.karyawan.model.Employee;
import id.co.dian.karyawan.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	@Override
	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Employee save(Employee param) {
		// TODO Auto-generated method stub
		if (param.getId() != null) {
			Employee get = repo.getOne(param.getId());
			param.setBirthDate(get.getBirthDate());
		}
		return repo.save(param);
	}

	@Override
	public Employee checkUnique(Employee param) {
		Employee cek = repo.getOne(param.getId());

		return cek;
	}

	@Override
	public void delete(Integer id) {
		repo.softDeleteById(id);

	}

	@Override
	public Employee getByNip(Integer number) {
		// TODO Auto-generated method stub
		return repo.getByIdNumber(number);
	}

	@Override
	public List<Employee> getAktif() {
		// TODO Auto-generated method stub
		return repo.getByAktif();
	}

}
