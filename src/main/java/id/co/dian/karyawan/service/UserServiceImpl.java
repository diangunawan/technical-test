package id.co.dian.karyawan.service;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.dian.karyawan.dto.UserDto;
import id.co.dian.karyawan.dto.UserRegistrasiDto;
import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.model.User;
import id.co.dian.karyawan.repository.GroupRepository;
import id.co.dian.karyawan.repository.UserRepository;
import id.co.dian.karyawan.sql.ConnectionSqlServer;

@Service
public class UserServiceImpl extends ConnectionSqlServer  implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

			User user = userRepository.findByUsername(username);
			if (user == null) {
				throw new UsernameNotFoundException("Invalid username or password." + username);
			}
			Group group = groupRepository.getById(user.getIdGroup());
			if (group == null) {
				throw new UsernameNotFoundException("Invalid username or password." + username);
			}
			Collection<Group> grps = new ArrayList<>();
			grps.add(group);
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
					mapRolesToAuthorities(grps));

	}

	@Override
	public User findByUsername(String username) {

		return userRepository.findByUsername(username);
	}

	@Override
	public User save(UserDto userDto) {
		User user = new User();
		Integer id = userRepository.getLastId();
		user.setIdUsers(userDto.getIdUsers() == null ? id+1 : userDto.getIdUsers());
		user.setEmail(userDto.getEmail());
		user.setIdGroup(userDto.getIdGroup());
		user.setUsername(userDto.getUsername());
		user.setPassword(passwordEncoder.encode(userDto.getPassword()));
		user.setTelp(userDto.getTelp());
		user.setLoginStatus(true);
		user.setCreated(new Date());
		user.setUserCreated(userDto.getUserCreated());
		return userRepository.save(user);
	}

	public Integer delete(Integer id) {
		try {
			connection = getConnection();
			PreparedStatement query = connection.prepareStatement("DELETE FROM al_users WHERE id_users="+id);
			Integer result = query.executeUpdate();
			//System.out.println(result);
			return result;
		}catch(Exception e){
			System.out.println(e);
			return 0;
		}
	}

	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Group> groups){
		return groups.stream()
				.map(group -> new SimpleGrantedAuthority(group.getNameGroup()))
				.collect(Collectors.toList());
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Integer getCountFilterUsers(String filterUserName, String filterGroup) {
		Integer rest = 0;
		try {
			connection = getConnection();
			String FilterName = " ";
			if(filterUserName != "") {
				FilterName = " AND a.username LIKE '%"+filterUserName+"%' ";
			}
			String queryGroup = " ";
			if(filterGroup != "") {
				queryGroup = " AND a.id_group="+filterGroup+" ";
			}
			PreparedStatement query = connection.prepareStatement("SELECT COUNT(a.id_users) as total \r\n"+
					"FROM al_users as a \r\n"+
					"JOIN al_group as b on b.id_group = a.id_group \r\n"+
					"WHERE 1=1 "+FilterName+" "+filterGroup);

			rs = query.executeQuery();
			while (rs.next()) {
				rest = Integer.parseInt(rs.getString("total"));
			}
			//System.out.println("total Filter "+result);
			return rest;
		}catch(HibernateException e){
			System.out.println(e);
			return rest;
		}catch(Exception e){
			System.out.println(e);
			return rest;
		}
	}

	public List<Object> getUsers(String filterUsername, String filterGroup, Integer start, Integer length) {
		List<Object> rest = new ArrayList<Object>();
		try {
			connection = getConnection();
			String queryUserName = " ";
			if(filterUsername != "") {
				queryUserName = " AND a.username LIKE '%"+filterUsername+"%' ";
			}
			String queryGroup = " ";
			if(filterGroup != "") {
				queryGroup = " AND a.id_group="+filterGroup+" ";
			}
			PreparedStatement query = connection.prepareStatement("SELECT a.id_users as id, a.username, b.name_group as nameGroup \r\n"+
					"FROM al_users as a \r\n"+
					"JOIN al_group as b on b.id_group = a.id_group \r\n"+
					"WHERE 1=1 "+queryUserName+" "+queryGroup+" "+
					"ORDER BY a.created DESC LIMIT ? OFFSET ? ");
			query.setInt(2, start);
			query.setInt(1, length);
			// show query error
			//System.out.println(query.toString());
			rs = query.executeQuery();

			Integer no = start;
			while (rs.next()) {
				Map<String,String> res = new HashMap<String,String>();
				no = no+1;
				res.put("no", no.toString());
				res.put("username", rs.getString("username"));
				res.put("groupName", rs.getString("nameGroup"));
				String btnEdit = "<a href=\"/user/edit/"+rs.getString("id")+"/\" class=\"btn btn-xs btn-success ajax\"><span class=\"fa fa-pencil\"></span></a>";
				String btnDelete = "<a data-href=\"/user/delete/"+rs.getString("id")+"/\" class=\"btn btn-xs btn-warning btn-delete\" title=\"Hapus data\" data-confirm=\"Anda yakin ingin menghapus Pengguna "+rs.getString("username")+"?\"><span class=\"fa fa-trash text-white\"></span></a>";
				res.put("action", "<div class=\"btn-group\">"+btnEdit+btnDelete+"</div>");

				rest.add(res);
			}
			return rest;
		}catch(HibernateException e){
			System.out.println(e);
			return rest;
		}catch(Exception e){
			System.out.println(e);
			return rest;
		}
	}

	public List<Object> getComboGroup(String search) {
		List<Object> rest = new ArrayList<Object>();
		try {
			connection = getConnection();
			String querySearch = " ";
			if(search != "") {
				querySearch = " AND name_group LIKE '%"+search+"%' ";
			}
			PreparedStatement query = connection.prepareStatement("SELECT id_group as id, name_group as text from al_group where 1=1"+querySearch);
			rs = query.executeQuery();
			while (rs.next()) {
				Map<String,String> res = new HashMap<String,String>();
				res.put("id", rs.getString("id"));
				res.put("text", rs.getString("text"));

				rest.add(res);
			}
			return rest;
		}catch(HibernateException e){
			System.out.println(e);
			return rest;
		}catch(Exception e){
			System.out.println(e);
			return rest;
		}
	}

}
